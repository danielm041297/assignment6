import os
import time
import re
from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode
from  decimal import Decimal
application = Flask(__name__, template_folder = "templates" )  # Change assignment here

app = application
"""
uswest oregon
ddmoorish-db
daniel
md228235
assignment4
"""

def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname

def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE Movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title VARCHAR(100), director VARCHAR(40), actor VARCHAR(40), release_date VARCHAR(40), rating DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("Table already exists.")
        else:
            print(err.msg)

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Error while creating table %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    message = ''
    year = request.form['year']
    title = request.form['title'].lower()
    director = request.form['director'].lower()
    actor = request.form['actor'].lower()
    release_date = request.form['release_date']
    rating = request.form['rating']
    if not title:
        message = 'Movie ' + title + ' could not be inserted - title field was empty'
        return hello(message)
    if len(title) > 100:
        message = 'Movie ' + title + ' could not be inserted - title too long'
        return hello(message)
    if not director:
        message = 'Movie ' + title + ' could not be inserted - director field was empty'
        return hello(message)
    if len(director) > 100:
        message = 'Movie ' + title + ' could not be inserted - director too long'
        return hello(message)
    if not actor:
        message = 'Movie ' + title + ' could not be inserted - actor field was empty'
        return hello(message)
    if len(actor) > 100:
        message = 'Movie ' + title + ' could not be inserted - actor too long'
        return hello(message)
    try:
        Decimal(rating)
    except Exception as exp:
        message = 'Movie ' + title + ' could not be inserted - rating field is not a decimal'
        return hello(message)
    if Decimal(rating) < 0 or Decimal(rating) > 10:        
        message = 'Movie ' + title + ' could not be inserted - invalid rating because it is not within 0 and 10.'
        return hello(message)

    
    pattern =  re.compile('^\d{4}$')
    result = pattern.match(year)
    if not result:
        message = 'Movie ' +title + ' could not be inserted - Invalid year input. Has to match YYYY'
        return hello(message)

    pattern = re.compile('^(([0-9])|([0-2][0-9])|([3][0-1]))\ (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\ \d{4}$')
    result = pattern.match(release_date)
    if not result:
        message = 'Movie ' +title + ' could not be inserted - Invalid release_date input. Has to match DD MMM YYYY where MMM is Jan, Feb, Mar,Apr,May ... Dec'
        return hello(message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    try:
        cur = cnx.cursor()
        select_stmt = "SELECT * FROM Movies WHERE title = %(title)s"
        cur.execute(select_stmt, { 'title': title })
        if cur.fetchone() != None:
            message = 'Movie ' +title + ' could not be inserted - already has movie with given title'
            return hello(message)

        insert_stmt = "INSERT INTO Movies (year,title,director,actor,release_date,rating) "+\
                    "VALUES (%s, %s, %s, %s, %s, %s)"
        data = (year, title, director, actor, release_date, rating)
        cur1 = cnx.cursor()
        cur1.execute(insert_stmt, data)
        cnx.commit()

    except Exception as exp:
        message = 'Movie ' + title + ' could not be inserted ' + str(exp)
        return hello(message)
    message = 'Movie '+ title + ' successfully inserted'
    return hello(message)
@app.route('/update_movie', methods=['POST'])
def update_movie():
    message = ''
    year = request.form['year']
    title = request.form['title'].lower()
    director = request.form['director'].lower()
    actor = request.form['actor'].lower()
    release_date = request.form['release_date']
    rating = request.form['rating']
    if not title:
        message = 'Movie ' + title + ' could not be updated - title field was empty'
        return hello(message)
    if len(title) > 100:
        message = 'Movie ' + title + ' could not be updated - title too long'
        return hello(message)
    if not director:
        message = 'Movie ' + title + ' could not be updated - director field was empty'
        return hello(message)
    if len(director) > 100:
        message = 'Movie ' + title + ' could not be updated - director too long'
        return hello(message)
    if not actor:
        message = 'Movie ' + title + ' could not be updated - actor field was empty'
        return hello(message)
    if len(actor) > 100:
        message = 'Movie ' + title + ' could not be updated - actor too long'
        return hello(message)
    try:
        Decimal(rating)
    except Exception as exp:
        message = 'Movie ' + title + ' could not be updated - rating field is not a decimal'
        return hello(message)
    if Decimal(rating) < 0 or Decimal(rating) > 10:        
        message = 'Movie ' + title + ' could not be updated - invalid rating because it is not within 0 and 10.'
        return hello(message)

    
    pattern =  re.compile('^\d{4}$')
    result = pattern.match(year)
    if not result:
        message = 'Movie ' +title + ' could not be updated - Invalid year input. Has to match YYYY'
        return hello(message)

    pattern = re.compile('^(([0-9])|([0-2][0-9])|([3][0-1]))\ (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\ \d{4}$')
    result = pattern.match(release_date)
    if not result:
        message = 'Movie ' +title + ' could not be updated - Invalid release_date input. Has to match DD MMM YYYY where MMM is Jan, Feb, Mar,Apr,May ... Dec'
        return hello(message)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    try:
        cur = cnx.cursor()
        select_stmt = "SELECT * FROM Movies WHERE title = %(title)s"
        cur.execute(select_stmt, { 'title': title })
        if cur.fetchone() == None:
            message = 'Movie ' +title + ' could not be updated - movie doesnt exist for title ' +title
            return hello(message)

        update_stmt = "UPDATE Movies SET year = %s, actor = %s, director = %s, release_date = %s, rating = %s WHERE title = %s"
        data = (year, actor, director, release_date, rating, title)
        cur1 = cnx.cursor()
        cur1.execute(update_stmt, data)
        cnx.commit()

    except Exception as exp:
        message = 'Movie ' + title + ' could not be upadted ' + str(exp)
        return hello(message)
    message = "Movie " + title + " successfully updated"
    return hello(message)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    title = request.form['delete_title'].lower()
    if not title:
        message = 'Movie ' + title + ' could not be deleted - title field was empty'
        return hello(message)
    if len(title) > 100:
        message = 'Movie ' + title + ' could not be deleted - title too long'
        return hello(message)
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    try:
        cur = cnx.cursor()
        select_stmt = "SELECT * FROM Movies WHERE title = %(title)s"
        cur.execute(select_stmt, { 'title': title })
        if cur.fetchone() == None:
            message = 'Movie ' +title + ' could not be deleted - movie doesnt exist for title ' +title
            return hello(message)
        delete_stmt = "DELETE FROM Movies WHERE title = %(title)s"
        cur.execute(delete_stmt, { 'title': title })
        cnx.commit()
    except Exception as exp:
        message = 'Movie ' + title + ' could not be deleted ' + str(exp)
        return hello(message)
    message = 'Movie ' + title +' successfully deleted'
    return hello(message)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    actor = request.args['search_actor'].lower()
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    message = ''
    messages = []
    try:
        cur = cnx.cursor()
        select_stmt = "SELECT * FROM Movies WHERE actor = %(actor)s"
        cur.execute(select_stmt, { 'actor': actor })
        movies = cur.fetchall()
        if movies == []:
            message = 'No movies found for actor ' + actor
            return render_template('index.html', message = message)
        for movie in movies:

            message = '<' + movie[2] +', '+ str(movie[1]) +', ' +movie[4]+ '>'
            messages.append(message)
    except Exception as exp:
        message = 'Could not search for movie with actor ' + actor +': ' + str(exp)
        return hello(message) 

    return render_template('index.html', messages = messages)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    message = ''
    messages = []
    try:
        cur = cnx.cursor()
        select_stmt = "SELECT title, year, actor, director, rating FROM Movies ORDER BY rating DESC"
        
        cur.execute(select_stmt)

        movies = cur.fetchall()
        high = None
        for movie in movies:
            if high ==None:
                print(movie[4])
                high = movie[4]
                message = '<' + movie[0] +', '+ str(movie[1]) +', ' +movie[2] +', ' +movie[3] +', ' +str(movie[4]) + '>'
                messages.append(message)
            elif high == movie[4]: #more than 1
                message = '<' + movie[0] +', '+ str(movie[1]) +', ' +movie[2] +', ' +movie[3] +', ' +str(movie[4]) +'>'
                messages.append(message)

    except Exception as exp:
        message = 'Could not search for movie with actor ' + actor +': ' + str(exp)
        return hello(message) 

    return render_template('index.html', messages = messages)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    message = ''
    messages = []
    try:
        cur = cnx.cursor()
        select_stmt = "SELECT title, year, actor, director, rating FROM Movies ORDER BY rating ASC"
        
        cur.execute(select_stmt)

        movies = cur.fetchall()
        low = None
        for movie in movies:
            if low ==None:
                print(movie[4])
                low = movie[4]
                message = '<' + movie[0] +', '+ str(movie[1]) +', ' +movie[2] +', ' +movie[3] +', ' +str(movie[4]) + '>'
                messages.append(message)
            elif low == movie[4]: #more than 1
                message = '<' + movie[0] +', '+ str(movie[1]) +', ' +movie[2] +', ' +movie[3] +', ' +str(movie[4]) + '>'
                messages.append(message)

    except Exception as exp:
        message = 'Could not search for movie with actor ' + actor +': ' + str(exp)
        return hello(message) 

    return render_template('index.html', messages = messages)


@app.route("/")
def hello(message = None):
    if not message:
        return render_template('index.html')    
    return render_template('index.html', message = message)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')